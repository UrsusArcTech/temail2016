﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEmail2016_Helper
{
    class HelperCommands
    {
        NetworkMessage nwMessage;
        public HelperCommands()
        {
            nwMessage = new NetworkMessage();
        }

        public bool parseAndSendCommands(string rawCommands)
        {
            string[] commands;
            Device device;

            try
            {
                commands = rawCommands.Split(' ');

                switch (commands[0])
                {
                    //this is for all devices in list
                    case "-a":
                        sendCommands(nwMessage.getDevices(), commands[1..(commands.Length)]);
                        break;
                    //this is for a specific device
                    case "-d":
                        device = nwMessage.getDeviceByName(commands[1]);
                        if (device != null)
                        {
                            sendCommands(device, commands[2..(commands.Length)]);
                        }
                        else
                        {
                            Console.WriteLine("Couldn't find device in list, sending anyway...");
                            sendCommands(new Device(commands[1]), commands[2..(commands.Length)]);
                        }
                        break;
                    case "-h":
                        writeHelp();
                        break;
                    default :
                        Console.WriteLine("Could not find -a, -d, or -h first.");
                        return false;
                        break;
                }

            }
            catch (System.Exception e)
            {
                Console.WriteLine("Error parsing commands.");
                return false;
            }

            return false;
        }

        async void sendCommands(List<Device> devices, string[] commands)
        {
            foreach (Device device in devices)
            {

                foreach(var command in commands)
                {
                    var response = await nwMessage.send(device.name, command);

                    if (response == "")
                    {
                        Console.WriteLine(device.name + ": - - - Offline - - - ");
                    }
                    else
                    {
                        Console.WriteLine(device.name + ": " + response);
                    }
                } 
            }
        }

        async void sendCommands(Device device, string[] commands)
        {
            foreach (var command in commands)
            {
                var response = await nwMessage.send(device.name, command);

                if (response == "")
                {
                    Console.WriteLine(device.name + ": - - - Offline - - - ");
                }
                else
                {
                    Console.WriteLine(device.name + ": " + response);
                }
            }
        }

        void writeHelp()
        {
            Console.WriteLine("-a Select all devices.");
            Console.WriteLine("-u Update TEmail client.");
            Console.WriteLine("-d Select a specific device, type the name after -d.");
            Console.WriteLine("-v Show TEmail client version.");
            Console.WriteLine("-hn Select PC host name on client.");
            Console.WriteLine("-c Clear all exceptions.");
            Console.WriteLine("");
            Console.WriteLine("Example: -a -v -hn will show all client versions and hostnames.");
            Console.WriteLine("Example: -a -u will update ALL clients.");
            Console.WriteLine("Example: -d mrpolycom_02 -u will update the client on mrpolycom_02.");

        }
    }
}
