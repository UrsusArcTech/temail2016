﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace TEmail2016
{
    class Calendar
    {
        Application oApp;
        NetworkMessage networkMessage;
        public Calendar(Application App, NetworkMessage nwMessage)
        {
            oApp = App;
            networkMessage = nwMessage;
        }


        public bool doesAppointmentConflict(AppointmentItem appointment)
        {
            bool conflict = false;
            try
            {
                Console.WriteLine("Getting appointment.");
                var start = appointment.Start;
                var end = appointment.End;

                var mapiNamespace = oApp.GetNamespace("MAPI");
                Console.WriteLine("Getting Calendar Folder.");
                var CalendarFolder = mapiNamespace.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);
               
                var outlookCalendarItems = CalendarFolder.Items;
                outlookCalendarItems.IncludeRecurrences = true;
                outlookCalendarItems.Sort("[Start]");
                
                Console.WriteLine("Checking conflict times.");
                
                
                foreach (AppointmentItem i in outlookCalendarItems.Restrict("[Start] >= '" + DateTime.Now.ToString("g") + "' AND [End] < '" + DateTime.Now.AddYears(2).ToString("g") + "'"))
                {
                    if ((start < i.End) && (i.Start < end))
                    {
                        //check if the appointment is cancelled
                        if (i.MeetingStatus != OlMeetingStatus.olMeetingCanceled && i.MeetingStatus != OlMeetingStatus.olMeetingReceivedAndCanceled && appointment.GlobalAppointmentID != i.GlobalAppointmentID)
                            conflict = true;
                    }

                }
            }
            catch(System.Exception e)
            {
                Console.WriteLine("Cannot check conflicts.");
                networkMessage.addException(new ExceptionNetwork(e.Message));
            }
            return conflict;
        }

        public void getAllCalendarItems()
        {
            
            NameSpace mapiNamespace = null;
            MAPIFolder CalendarFolder = null;
            Items outlookCalendarItems = null;

            mapiNamespace = oApp.GetNamespace("MAPI");
            CalendarFolder = mapiNamespace.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);
            outlookCalendarItems = CalendarFolder.Items;
            outlookCalendarItems.IncludeRecurrences = true;

            foreach (AppointmentItem item in outlookCalendarItems)
            {

                    Console.WriteLine(item.Subject + " -> " + item.Start.ToLongDateString());
                
            }
        }
    }
}
