﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Collections.Generic;
namespace TEmail2016
{
    class MailHandling
    {
        public static string caEmailAdd;
        public static string comEmailAdd;
        string meetingReqType = "IPM.Schedule.Meeting.Request";
        NetworkMessage networkMessage;

        public MailHandling(NetworkMessage nwMessage)
        {
            networkMessage = nwMessage;
        }


        public void scanMailbox()
        {
            try
            {
                Application oApp = new Application();
                NameSpace nameSpace = oApp.GetNamespace("MAPI");
                Console.WriteLine("Scanning...");
                var inboxFolder = oApp.Session.GetDefaultFolder(OlDefaultFolders.olFolderInbox);
                caEmailAdd = System.DirectoryServices.AccountManagement.UserPrincipal.Current.UserPrincipalName;
                comEmailAdd = oApp.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;

                List<string> recipients = new List<string>();
                var cal = new Calendar(oApp, networkMessage);
                MeetingItem meetingItem;
                AppointmentItem apptItem;

                //loop through all mailbox items
                foreach (object item in inboxFolder.Items)
                {
                    try
                    {
                        //select meeting items
                        if (item is MeetingItem)
                        {
                            Console.WriteLine("Getting meeting item.");
                            meetingItem = ((MeetingItem)item);
                            apptItem = meetingItem.GetAssociatedAppointment(false);

                            //is this a meeting request?
                            if (((MeetingItem)item).FormDescription.MessageClass == meetingReqType)
                            {
                                Console.WriteLine("Checking conflict.");
                                Console.WriteLine(apptItem.Start);
                                //check if it conflicts with anything in the calendar
                                if (cal.doesAppointmentConflict(apptItem) == true)
                                {
                                    Console.WriteLine("Conflict found.");
                                    //if so decline and respond to sender
                                    apptItem.Respond(OlMeetingResponse.olMeetingDeclined).Send();
                                    Console.WriteLine("Deleting...");
                                    Console.WriteLine("Sender:");
                                    Console.WriteLine(((MeetingItem)item).SenderEmailAddress);
                                    ((MeetingItem)item).Delete();
                                    if (item != null)
                                        Marshal.ReleaseComObject(item);
                                }
                                else
                                {
                                    Console.WriteLine("No Conflict Found.");
                                    //if not send to ca
                                    recipients.Add(caEmailAdd);
                                    //respond to meeting request
                                    apptItem.Respond(OlMeetingResponse.olMeetingAccepted).Send();
                                    sendMail(meetingItem, recipients);
                                    recipients.Clear();
                                }
                            }
                            //not a meeting request, so delete the email
                            else
                            {
                                Console.WriteLine("Deleting...");
                                Console.WriteLine("Sender:");
                                Console.WriteLine(((MeetingItem)item).SenderEmailAddress);
                                ((MeetingItem)item).Delete();
                                if (item != null)
                                    Marshal.ReleaseComObject(item);
                            }
                        }


                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine("Overall mail scanning error:");
                        Console.WriteLine(e);
                        networkMessage.addException(new ExceptionNetwork(e.Message));

                        try
                        {
                            Console.WriteLine("Deleting...");
                            Console.WriteLine("Sender:");
                            Console.WriteLine(((MeetingItem)item).SenderEmailAddress);
                            ((MeetingItem)item).Delete();
                            if (item != null)
                                Marshal.ReleaseComObject(item);
                        }
                        catch (System.Exception ex)
                        {
                            Console.WriteLine("Error deleting in exception:");
                            Console.WriteLine(ex);
                            networkMessage.addException(new ExceptionNetwork(e.Message));
                        }
                    }

                    if (item != null)
                        Marshal.ReleaseComObject(item);
                }

            }catch(System.Exception e)
            {
                Console.WriteLine("Error over all of send:");
                Console.WriteLine(e.Message);
                networkMessage.addException(new ExceptionNetwork(e.Message));
            }

        }


        private void sendMail(MeetingItem item, List<string> recipients)
        {
            MeetingItem meetingForward;
            
            if (item is MeetingItem)
            {
                meetingForward = ((MeetingItem)item).Forward();

                Console.WriteLine("Sending...");
                try
                {
                    //delete all recipiemts
                    foreach (Recipient rec in meetingForward.Recipients)
                    {
                        rec.Delete();
                    }
                    //get new recipients
                    foreach (var recipient in recipients)
                    {
                        meetingForward.Recipients.Add(recipient);
                    }
                    //send this mail
                    meetingForward.Send();
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("Error sending message: ");
                    Console.WriteLine(e);
                    networkMessage.addException(new ExceptionNetwork(e.Message));

                }

                //delete item
                Console.WriteLine("Deleting after send/can't send...");
                if(item != null)
                    ((MeetingItem)item).Delete();
                if (item != null)
                    Marshal.ReleaseComObject(item);
            }
        }

        private  void ReleaseComObject(object obj)
        {
            if (obj != null)
                Marshal.ReleaseComObject(obj);
        }
    }
}
